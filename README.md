# README #

### RoboChat Test ###

* The purpose of this repository is to act as a playground for exploring ReactJS and the various frameworks typically used in combination with React, such as Flux etc.
* 0.0.1

### How do I get set up? ###

* Summary of set up
To get setup, all you need to do is ensure that your environment has the Node CLI tools installed. Using the command line, navigate to the source codes root directory. Next run `npm install`. Once complete you can start the server up by simply running `node server.js`. Assuming everything is awesome, you can then access the application using the browser of your choice [by clicking here](http://localhost:3000/).

### Who do I talk to? ###

* Dean Boonzaier
* Abdul Rahim Allana