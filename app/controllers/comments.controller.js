'use strict';

var fs = require('fs'),
  path = require('path'),
  COMMENTS_FILE = path.join('', 'comments.json'),
  request = require('request'),
  lodash = require('lodash');

exports.find = function (req, res) {
  fs.readFile(COMMENTS_FILE, function (err, data) {
    if (err) {
      console.error(err);
      process.exit(1);
    }
    res.json(JSON.parse(data));
  });
};

exports.getComments = function(req, res) {
  fs.readFile(COMMENTS_FILE, function (err, data) {
    if (err) {
      console.error(err);
      process.exit(1);
    }

    res.json(JSON.parse(data));
  });
}

exports.saveComment = function (req, res) {
  console.log(req);
  fs.readFile(COMMENTS_FILE, function (err, data) {
    if (err) {
      console.error(err);
      process.exit(1);
    }
    var comments = JSON.parse(data);
    // NOTE: In a real implementation, we would likely rely on a database or
    // some other approach (e.g. UUIDs) to ensure a globally unique id. We'll
    // treat Date.now() as unique-enough for our purposes.
    console.log(req.body);
    var newComment = {
      id: Date.now(),
      authorName: req.body.authorName,
      text: req.body.text,
      timestamp: Date.now()
    };
    comments.push(newComment);
    fs.writeFile(
      COMMENTS_FILE,
      JSON.stringify(comments, null, 4),
      function (err) {
        if (err) {
          console.error(err);
          process.exit(1);
        }
        res.json(comments);
      }
    );
  });
};

exports.getRandomChuckism = function (req, res) {
  request(
    'http://api.icndb.com/jokes/random',
    function (error, getResp, getRespData) {
      if (error) {
        res.status(400).send({details: error});
      }
      else if (lodash.isEqual(getResp.statusCode, 200)) {
        var jsonResponse = JSON.parse(getRespData);

        fs.readFile(COMMENTS_FILE, function (err, data) {
          if (err) {
            console.error(err);
            process.exit(1);
          }
          var comments = JSON.parse(data);

          var newComment = {
            id: Date.now(),
            authorName: 'RoboChat',
            text: lodash.get(jsonResponse, 'value.joke', 'Oh noes, something is wrong. No Chuckisms are available!'),
            timestamp: Date.now()
          };

          comments.push(newComment);
          fs.writeFile(
            COMMENTS_FILE,
            JSON.stringify(comments, null, 4),
            function (err) {
              if (err) {
                console.error(err);
                process.exit(1);
              }
              res.jsonp(newComment);
            }
          );
        });
      }
    }
  );
};
