'use strict';

module.exports = function(app) {
    var commentsController = require('../controllers/comments.controller.js');

    app.route('/api/comments').
        get(commentsController.getComments).
        post(commentsController.saveComment);

    app.route('/api/comments/response').
      get(commentsController.getRandomChuckism)

};
