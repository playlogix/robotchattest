var request = require('superagent');

module.exports = {
  getMessages: function () {
    return new Promise(function (resolve, reject) {
      request
        .get('http://localhost:3000/api/comments')
        .set('Accept', 'application/json')
        .end(function (error, response) {
          console.log(response);
          if (response.status === 404) {
            reject();
          } else {
            resolve(JSON.parse(response.text));
          }
        });
    });
  },
  createMessage: function (message) {
    return new Promise(function (resolve, reject) {
      request
        .post('http://localhost:3000/api/comments')
        .set('Accept', 'application/json')
        .send(message)
        .end(function (error, response) {
          console.log(response);
          if (response.status === 404) {
            reject();
          } else {
            resolve(JSON.parse(response.text));
          }
        });
    });
  },
  getRobotResponse: function() {
    return new Promise(function (resolve, reject) {
      request
        .get('http://localhost:3000/api/comments/response')
        .set('Accept', 'application/json')
        .end(function (error, response) {
          console.log(response);
          if (response.status === 404) {
            reject();
          } else {
            resolve(JSON.parse(response.text));
          }
        });
    });
  }
};
